import { createStore, applyMiddleware, compose } from "redux";

import reducer from '../reducers/';
import thunk from 'redux-thunk';

import promise from './middleware/promise'

// Проверяем мод и если это не продакшн, подключаем redux-dev-tools
const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const middleware = applyMiddleware(
    thunk,
    promise,
);


const store = createStore( reducer,  composeEnhancers( middleware ) );

export default store;
