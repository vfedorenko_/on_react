import { combineReducers } from 'redux';

import movieListReducer from './movielist';
import ganreListReducer from './genrelist';
import filmListReducer from './film';

const reducer = combineReducers({
    movieListReducer,
    ganreListReducer,
    filmListReducer,
});

export default reducer;