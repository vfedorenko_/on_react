import {
    GET_GENRE_LIST_REQ,
    GET_GENRE_LIST_RES,
    GET_GENRE_LIST_ERROR,
} from '../actions';

const ganreInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: [],
};

const ganreListReducer = ( state = ganreInitialState, action) => {
    switch( action.type ){

        case GET_GENRE_LIST_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_GENRE_LIST_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        case GET_GENRE_LIST_ERROR:
            return{
                ...state,
                loaded: false,
                loading: false,
                errors: action.errors,
            }

        default:
            return state;
    }
}

export default ganreListReducer;