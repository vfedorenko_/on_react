import React from 'react';
import { IntlProvider } from 'react-intl';
import App from './App';

import translations from './translations';

import { defineLanguage } from './helpers/defineLanguage';

export const IntlComponent = React.memo( ( props ) => {
    const locale = defineLanguage();
    const setled = props.match.params.locale;
    // console.log(props, locale);

    return(
        <IntlProvider 
            key={setled} 
            locale={setled}
            defaultLocale={locale}
            messages={translations[setled]}
        >
            <App {...props}/>
        </IntlProvider>
    )
} );