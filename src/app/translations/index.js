import config from './config';

import home from './home';
import genres from './genres';
import search from './searchbar';
import navigation from './navigation';
import card from './card';

const allSupportedDictionaries = [
    home,
    genres,
    search,
    navigation,
    card,
];

let connectedTranslations = {};
config.supprotedLangs.forEach( lang => connectedTranslations[lang] = {} );

allSupportedDictionaries.forEach( dics => {
    config.supprotedLangs.forEach( lang => {
        connectedTranslations[lang] = {
            ...connectedTranslations[lang],
            ...dics[lang],
        };
    } );
} );

export default connectedTranslations;