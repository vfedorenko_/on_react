export default {
    uk: {
        'home.title': 'Домівка',
    },
    en: {
        'home.title': 'Home Page',
    },
    it: {
        'home.title': 'Pagina di Casa',
    },
}