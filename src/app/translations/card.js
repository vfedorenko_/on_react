export default {
    uk: {
        'card.title': 'Назва: ',
        'card.releas': 'Вийшов: ',
        'card.pop': 'поп.: ',
        'card.original': 'Оригінальна назва: ',
        'card.vote': 'Оцінка: ',
        'card.overview': 'Опис: ',
    },
    en: {
        'card.title': 'Title: ',
        'card.releas': 'Relesed: ',
        'card.pop': 'pop.: ',
        'card.original': 'Original title: ',
        'card.vote': 'Vote average: ',
        'card.overview': 'Overview: ',
    },
    it: {
        'genres.title': 'Titolo: ',
        'genres.releas': 'è uscito: ',
        'genres.pop': 'pop.: ',
        'genres.original': 'Titolo originale: ',
        'genres.vote': 'Voto modesto: ',
        'card.overview': 'Deskrizione: ',
    },
}