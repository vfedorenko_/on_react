export default {
    uk: {
        'navigation.home': 'Додому',
        'navigation.genres': 'Жанри',
    },
    en: {
        'navigation.home': 'Home',
        'navigation.genres': 'Genres',
    },
    it: {
        'navigation.home': 'Casa',
        'navigation.genres': 'Generi',
    },
}