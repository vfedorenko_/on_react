import React from 'react';
import { NavLink } from 'react-router-dom';

export const I10nNavLink = ( { to, locale, children, ...props } ) => {
    let langPath = '';

    if (typeof(to) === 'object') {
        langPath = {
            ...to,
            pathname: `/${locale}${to.pathname}`
        } 
    } else {
        langPath = `/${locale}${to}`
    }

    return(
        <NavLink 
            to={langPath}
            {...props}
        >
            { children }
        </NavLink>
    )

}