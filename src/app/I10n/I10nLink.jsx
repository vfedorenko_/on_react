import React from 'react';
import { Link } from 'react-router-dom';

export const I10nLink = ( { to, locale, children, ...props } ) => {
    let langPath = '';

    if (typeof(to) === 'object') {
        langPath = {
            ...to,
            pathname: `/${locale}${to.pathname}`
        } 
    } else {
        langPath = `/${locale}${to}`
    }

    return(
        <Link 
            to={langPath}
            {...props}
        >
            { children }
        </Link>
    )

}