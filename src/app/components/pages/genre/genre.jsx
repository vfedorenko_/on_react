import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';
import React from 'react';

import { getGanreListPromise } from '../../../actions';
import GenreId from './genreid';

import { SideBar, Loader } from '../../ui';

import './genre.css';

class Genre extends React.PureComponent {

    componentDidMount = () => {
        const { loaded } = this.props.list;
        const { getAllFilms } = this.props;
        const { locale } = this.props.match.params;

        if (!loaded) {
            getAllFilms(locale);
        }
    }


    render = () => {
        const { loaded, data } = this.props.list;
        const { url } = this.props.match;

        return(
            <>
                <h3  className="genre-title"><FormattedMessage id="genres.title"/></h3>
                <div className="genre-container">
                    {
                        (loaded) ? 
                            (
                                <>
                                    <SideBar list={data.genres} url={url} />
                                    <Switch>
                                        <Route path="/:langPath/genre/:genreid" component={GenreId}/>
                                    </Switch>
                                </>
                            )
                            : <Loader />
                    }
                    
                    
                    
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    list: state.ganreListReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllFilms: (lang) => {
        dispatch( getGanreListPromise(lang) );
    }
})

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Genre));