import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Catalog from '../catalog/catalog';

const GanreId = ({ match }) => {
    let url = match.url;

    return(
        <div className="catlog-container">
            <h1>Genre ID Page</h1>
            <Switch>
                <Route exact path="/:locale/genre/:genreid/:pageid" component={Catalog}/>
                <Redirect to={`${url}/1`} />
            </Switch>
        </div>
    )
} 

export default GanreId;