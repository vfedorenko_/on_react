import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getMovieListPromise } from '../../../actions';
import { Paginator, Card, Loader } from '../../ui';

import { PATH } from '../../../constants';

import './catalog.css';

class Catalog extends React.Component {

    componentDidMount = () => {
        const { getAllFilms } = this.props;

        getAllFilms(
            this.chooseRequest(),
            this.props.match.params.pageid,
            this.props.match.params.genreid,
            this.props.match.params.what,
            this.props.match.params.locale
        );
        
    }

    bodyPath = () => {
        let path = this.props.location.pathname;
        return path.split('/')[2];
    }

    chooseRequest = () => {
        let path = this.bodyPath();

        if ( path ) {
            return PATH[path];
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { getAllFilms } = this.props;
        const { pageid } = prevProps.match.params;
        const { params } = this.props.match;

        if (pageid !== params.pageid) {

            getAllFilms(
                this.chooseRequest(),
                this.props.match.params.pageid,
                this.props.gen,
                this.props.query,
                this.props.match.params.locale
            );

        }

    }

    render() {
        const { list, match } = this.props;
        const page = +this.props.match.params.pageid;
        const path = `${this.props.match.params.locale}/${this.bodyPath()}/`;

        return (
            <div className="catal-container">
                {
                    list.loaded 
                        && <Paginator page={page} total={list.data.total_pages} bodyPath={path} />
                }
                {
                    list.loaded 
                        ? <div className="catalog">
                            { 
                                list.data.results.map( item => {
                                    {/* let path = `${match.params.locale}/film/${item.id}` */}
                                    return(
                                        <Link 
                                            key={item.id} 
                                            replace 
                                            to={'/' + match.params.locale + '/film/' +item.id}
                                        >
                                            <Card 
                                                key={item.id}
                                                src={"https://image.tmdb.org/t/p/w500/" + item.poster_path }
                                                {...item}
                                            />
                                        </Link>
                                    ) 
                                } )
                            } 
                        </div>
                    : <Loader />
                }
                {
                    list.loaded 
                        && <Paginator page={page} total={list.data.total_pages} bodyPath={path} />
                }
            </div>
        )

    }
    
}

const mapStateToProps = (state) => ({
    list: state.movieListReducer,
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllFilms: (url, page, genre, query, lang) => {
        dispatch( getMovieListPromise(url, page, genre, query, lang) );
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);