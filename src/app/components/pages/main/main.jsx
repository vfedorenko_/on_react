import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
// eslint-disable-next-line
import { Switch, Route, Redirect } from 'react-router-dom';

import Mainpag from '../catalog/catalog';

const Main = () => {
    return(
        <>
            <h2><FormattedMessage id="home.title"/></h2>
            <Switch>
                <Route exact path="/:locale/main/:pageid" component={Mainpag} />
            </Switch>
        </>
    )
}

export default injectIntl(Main);