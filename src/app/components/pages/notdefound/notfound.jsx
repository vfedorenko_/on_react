import React from 'react';

import './notfound.css';

const NotFound = () => (
    <div className='not-found'>
        <h3 className='not-title'>Error 403: Page not exist.</h3>
    </div>
)

export default NotFound;