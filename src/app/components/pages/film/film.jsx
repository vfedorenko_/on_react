import React from 'react';
import { connect } from 'react-redux';

import { getMoviePromise } from '../../../actions';
import { Loader, FilmPag } from '../../ui';

// import 'film.css';

class Film extends React.Component {

    componentDidMount() {
        const { id, locale } = this.props.match.params;
        this.props.getFilm( id, locale );
    }

    render () {
        const { data, loaded } = this.props.film;
        const { locale } = this.props.match.params;
        // console.log(film);

        return (
            <>
                {
                    loaded 
                        ? <FilmPag 
                            data={data} 
                            baseUrl={'/' + locale + '/genre/'}
                        />
                        : <Loader />
                }
            </>
        )
    }
    
}

const mapStateToProps = (state) => ({
    film: state.filmListReducer,
    // genre: state.ganreListReducer
});

const mapDispatchToProps = ( dispatch ) => ({
    getFilm: ( id, lang ) => {
        dispatch( getMoviePromise( id, lang ) );
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Film);