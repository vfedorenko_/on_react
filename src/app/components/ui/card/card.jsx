import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import './card.css';

export const Card = React.memo( injectIntl( props => (

    <section className="card">
        <div className="card-header">
            <h5 className="card-title">{props.title}</h5>
            <p className="card-text"><FormattedMessage id="card.releas"/> {props.release_date}</p>
            <p className="card-text"><FormattedMessage id="card.pop"/> {props.popularity}</p>
        </div> 
        <img className="card-img-top" src={props.src} alt='a film cover' />
        <div className="card-body">
            <h6 className="card-title"><FormattedMessage id="card.original"/>{props.original_title}</h6>
        </div>
    </section>

) ) );