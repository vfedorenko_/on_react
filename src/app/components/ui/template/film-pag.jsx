import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import './film-pag.css';

export const FilmPag = injectIntl(( props ) => {
    const { data, baseUrl } = props;

    return(
        <div className="card temp">
            <div className="card-header">
                <h1 className="card-title temp">
                    {data.title}
                </h1>
            </div> 
            <div className="row">
                <img 
                    className="image" 
                    src={"https://image.tmdb.org/t/p/w500/" + data.poster_path}
                    alt="film poster" 
                />

                <div className="content">
                    <p className="card-text">
                        <span><FormattedMessage id="card.title"/></span>
                        { data.title }
                    </p>
                    <p className="card-text">
                        <span><FormattedMessage id="card.original"/></span>
                        { data.original_title }
                    </p>
                    <p className="card-text">
                        <span><FormattedMessage id="card.releas"/></span>
                        { data.release_date }
                    </p>
                    <p className="card-text">
                        <span><FormattedMessage id="card.pop"/></span>
                        { data.popularity }
                    </p>
                    <p className="card-text">
                        <span><FormattedMessage id="card.vote"/></span>
                        { data.vote_average + '/10' }
                    </p>
                </div>
                <div className="overview">
                    <p className="card-text">
                        <span><FormattedMessage id="card.overview"/></span>
                    </p>
                    <p className="card-text">
                        { data.overview }
                    </p>
                </div>
            </div>
            <div className="card-footer">
                {
                    data.genres.map( item => {
                        return(
                            <Link 
                                key={item.id}
                                className="foot-link"
                                to={baseUrl + item.id}
                            >
                                {
                                    item.name
                                }
                            </Link>
                        )
                    } )
                }
            </div>
        </div>
    )
});