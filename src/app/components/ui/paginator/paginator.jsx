import React from 'react';
import { Link } from 'react-router-dom';

import './paginator.css';

export const Paginator = React.memo( props => {
    // eslint-disable-next-line
    // const handl = ( iter ) => ( e ) => {
    //     console.log(props.total)
    //     let page = {
    //         page: iter,
    //         tot: props.total,
    //     }

    //     props.handler( page );
    // } 

    const { page, total, bodyPath } = props;
    const path = bodyPath;
    let prev = `${page - 1}`;
    let next = `${page + 1}`;
    let last = '' + total;

    return(
        <div className="paginator">
            {
                (page > 1) && <Link className="pag-link" to={path}>begin</Link>
            }
            {
                (page > 2) && <Link className="pag-link" to={prev}>prev</Link>
            }

            {
                (page > 3) && <p className="pag-no-link" >...</p>
            }
                
            {
                (page > 1) && <Link className="pag-link" to={prev}>{page - 1}</Link>
            }
                <p className="pag-no-link" >{page}</p>
            {
                (page < total) && <Link className="pag-link"  to={next}>{page + 1}</Link>
            }
            
            {
                (page < total - 2) && <p className="pag-no-link" >...</p>
            }
                
            {
                (page < total - 1) && <Link className="pag-link"  to={next}>next</Link>
            }
            {
                (page < total) && <Link className="pag-link" to={last}>last</Link>
            }
        </div>
    )
} );