import React from 'react';
import { I10nLink } from '../../../I10n';

import config from '../../../translations/config';

import './langswitcher.css';

export const LangSwitcher = React.memo( props => {
    const { supprotedLangs } = config;
    const oldPath = props.path;
    let newPath = oldPath.slice(3);
    // console.log(oldPath, newPath);

    return(
        <div className="lang-switcher">
            {
                supprotedLangs.map( item => (
                        <I10nLink 
                            key={item} 
                            to={newPath} 
                            locale={item}
                            className={'lang-item'}
                        > 
                            {item} 
                        </I10nLink>
                    )
                )
            }
        </div>
    )
});