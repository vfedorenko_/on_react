import React from 'react';
import { Link } from 'react-router-dom';

import './sidebar.css';


export const SideBar = props => {
    const { list, url } = props;

    return (
        <div className="sidebar">
            {
                list.map( itm => {
                    let genreid = itm.id;
                    return( 
                        <Link 
                            className='side-item' 
                            replace 
                            key={itm.id} 
                            to={url + '/' + genreid}
                        >
                            {
                                itm.name
                            }
                        </Link> 
                    );
                } )
            }
        </div>
    )
};

