import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import { I10nNavLink } from '../../../I10n';

import { SearchBar } from '../';
import { LangSwitcher } from '../';

import './header.css';

export const Header = React.memo( injectIntl( props => {
    const locale = props.match.params.locale;
    const pathname = props.location.pathname;

    return(
        <header className="App-header">

            <nav className='header-menu'>
                <I10nNavLink className='nav-item' to='/main/1' locale={locale}><FormattedMessage id="navigation.home"/></I10nNavLink>
                <I10nNavLink className='nav-item' to='/genre' locale={locale}><FormattedMessage id="navigation.genres"/></I10nNavLink>
            </nav>

            <div className='header-side'>
                <LangSwitcher path={pathname} />
                <hr />
                <SearchBar />
            </div>

        </header>
    )
} ) );