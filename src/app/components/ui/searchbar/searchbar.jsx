import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { I10nLink } from '../../../I10n';

import './searchbar.css';

class SearchBarClass extends React.PureComponent {
    state ={
        value: ''
    }

    handler = (e) => {
        e.preventDefault();
        this.setState({
            value: e.target.value
        });
        // console.log(this.state.value);
    }

    render = () => {
        const { value } = this.state;
        const { intl } = this.props;

        return (
            <form action="#" method='GET' className="search-form">
                <label htmlFor="search" className="screen-reader-text">Search</label>
                <input id='search' type="search" className='search-input' placeholder={intl.formatMessage({ id: 'search.enter' })} value={value} onChange={this.handler}/>
                <I10nLink className="search-button" to={"/search/" + value + '/1'} locale={intl.locale}><FormattedMessage id="search.button"/></I10nLink>
            </form>
        )
    }
}

export const SearchBar = injectIntl( props => (
    <SearchBarClass {...props} />
));