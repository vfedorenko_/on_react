export * from './card/card';
export * from './header/header';
export * from './langswitcher/langswitcher';
export * from './loader/loader';
export * from './paginator/paginator';
export * from './searchbar/searchbar';
export * from './sidebar/sidebar';
export * from './template/film-pag';
