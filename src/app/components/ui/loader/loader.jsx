import React from 'react';

import './loader.css';

export class Loader extends React.Component {
    state = {
        load: false,
    }

    nIntervId = null;

    changeState() {
        console.log('time')
        let newstate = this.state.load ? false : true;
        this.setState( {
            load: newstate
        } );
    }

    componentDidMount = () => {
        this.nIntervId = setInterval(() => {
            this.changeState()
        }, 3000);
    }

    componentWillUnmount = () => {
        clearInterval(this.nIntervId);
    }

    render = () => {
        const { load } = this.state;

        return (
            <div className='loader'>
                <div className={
                    load ? 
                        "action" : 
                        "loader-box"
                }>
                    <p className='loader-text'>
                        Loading...
                    </p>
                </div>
            </div>
        );
    }
}
