export const PATH = {
    main: '/movie/popular',
    genre: '/discover/movie',
    search: '/search/movie',
}