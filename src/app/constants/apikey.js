
export const APIKEY = '?api_key=9ee7d0842ddc6349c1b3b867c083b341&language=en-US';
export const API_KEY = '?api_key=9ee7d0842ddc6349c1b3b867c083b341';
export const GENRE = '&sort_by=popularity.desc&include_adult=false&include_video=false&with_genres=';
export const PAGE = '&page=';
export const LANG = '&language=';
export const SEARCH = '&query=';

export const LANGUAGES = {
    uk: '',
    en: '',
    it: '',
    de: '',
}

// for serch => https://api.themoviedb.org/3/search/movie
// ?api_key=9ee7d0842ddc6349c1b3b867c083b341
// &language=en-US
// &query=dark      => search value
// &page=1&include_adult=false