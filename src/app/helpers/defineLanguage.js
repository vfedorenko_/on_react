import defaults from '../translations/config';

export const defineLanguage = (  ) => {
    let userSystemLanguage = navigator.language;
    userSystemLanguage = userSystemLanguage.split('-')[0];
    // let presetLang = 
    // console.log(userSystemLanguage);

    if (!defaults.supprotedLangs) {
        return defaults.defaultLanguage;
    }
    let locale = defaults.supprotedLangs.filter( lang => ( lang === userSystemLanguage ) );
    if (locale) {
        return locale[0];
    } else {
        return defaults.defaultLanguage;
    }
}