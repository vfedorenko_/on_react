import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Header } from './components/ui'

import { ROUTES } from './Router';

import './App.css';

const App = React.memo( props => {

    return (
        <div className="App">
            
            <Header {...props}/>
            
            <Switch>
                {
                    ROUTES.map( (route, index) => (
                        <Route 
                            key={index}
                            { ...route }
                        />
                    ) )
                }
                <Redirect to={`/:locale/main/1`} />
            </Switch>

        </div>
    );
} );

export default App;
