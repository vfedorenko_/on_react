import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { IntlComponent } from './IntlComponent';

import store from './redux/store';

import config from './translations/config';
import { defineLanguage } from './helpers/defineLanguage';

const SupportedLangs = config.supprotedLangs.join('|');
const locale = defineLanguage();

const Wrapper = React.memo( props => (
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path={`/:locale(${SupportedLangs})`} component={IntlComponent} />
                <Redirect to={`${locale}/main/1`} />
            </Switch>
        </BrowserRouter>
    </Provider>
) );

export default Wrapper;