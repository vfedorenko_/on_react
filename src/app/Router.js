import NotFound from './components/pages/notdefound/notfound';
import Main from './components/pages/main/main';
import Genre from './components/pages/genre/genre';
import Film from './components/pages/film/film';
import Catalog from './components/pages/catalog/catalog';

export const ROUTES = [
    {
        path: '/:locale/main/:pageid',
        component: Main,
        exact: true
    },
    {
        path: '/:locale/genre/',
        component: Genre,
        exact: false
    },
    {
        path: '/:locale/film/:id',
        component: Film,
        exact: true
    },
    {
        path: '/:locale/search/:what/:pageid',
        component: Catalog,
        exact: true
    },
    {
        component: NotFound
    }
]
